<?php

// creates pages dir
$dir = __DIR__ . '/public';
if (file_exists($dir) === false) {
	mkdir($dir, 0777, true);
}

// starts getting HTML output
ob_start();
?><!doctype html>
<html lang="pl">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NCN Project No. 2016/21/B/ST6/02158</title>
    <link rel="canonical" href="http://2016-21-b-st6-02158.gitlab.io/">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;700&display=swap" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
	<style>
		body {
			background: url(images/bg.png) #D96C57;
			padding-top: 60px;
			padding-bottom: 45px;
			font-family: 'Roboto Slab', serif;
		}
		.container.main {
			background-color: #fff;
			border-radius: 10px;
			overflow: hidden;
			padding-left: 30px;
			padding-right: 30px;
		}
		.container.main > .navbar {
			margin: 0 -30px 15px;
		}
		.container.main .display-4 {
			font-size: 2rem;
			padding-bottom: 10px;
			padding-right: 45px;
			border-bottom: 2px solid #317671;
			margin-bottom: 15px;
			margin-top: 15px;
			float: left;
		}
		.container.main .display-4.first {
			margin-top: 0px;
		}
		body > .display-4 {
			text-align: center;
			padding-bottom: 45px;
			line-height: 1.1;
		}
		body > .display-4 small {
			font-weight: 300;
		}
		p, ul, .row, ol {
			clear: left;
		}
		span.sc {
			font-variant: small-caps;
		}
		h3 {
			margin-bottom: 15px;
		}
		.investigators {
			padding-top: 15px;
			padding-bottom: 15px;
		}
		.investigators p {
			margin-bottom: 10px;
		}
		.investigators p + p {
			margin-bottom: 15px;
		}
		ol li + li {
			margin-top: 10px;
		}
		a {
			color: #A52254;
		}
		footer {
			margin-top: 15px;
		}
		footer .container {
			padding: 0;
		}
		.back-to-top {
			position: fixed;
			bottom: 25px;
			right: 25px;
			display: none;
		}
		pre {
			margin: 0;
		}
    </style>
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<h1 class="display-4">
		Project No. 2016/21/B/ST6/02158<br>
		<small>Supported by National Science Centre Poland (NCN)</small>
	</h1>
	<div class="container main">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="#about">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#objectives">Objectives</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#research-agenda">Research agenda</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#investigators">Investigators</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#publications">Publications</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#summary">Summary</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contact">Contact</a>
				</li>
			</ul>
		</nav>
		<div class="row">
			<div class="col">
				<a name="about"></a>
				<h2 class="display-4 first"><i class="fas fa-angle-right"></i> About</h2>
				<?php include __DIR__ . '/content/about.html'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="objectives"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Research project objectives</h2>
				<?php include __DIR__ . '/content/objectives.html'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="research-agenda"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Research agenda</h2>
				<?php include __DIR__ . '/content/research-agenda.html'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="investigators"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Investigators</h2>
				<div class="row investigators">
				<?php
					$investigators = json_decode(file_get_contents(__DIR__ . '/content/investigators.json'), true);
					foreach ($investigators as $investigator) {
						echo '<div class="col-lg-4 col-xl-3 text-center">';
						if (is_null($investigator['url'])) {
							echo '<h4>' . $investigator['name'] . '</h4>';
						} else {
							echo '<h4><a href="' . $investigator['url'] . '" target="_blank">' . $investigator['name'] . '</a></h4>';
						}
						echo '<p>' . $investigator['title'] . '</p>';
						echo '<p>' . $investigator['employing'] . '</p>';
						echo '</div>';
					}
				?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="publications"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Publications</h2>
				<ol>
				<?php
					$publications = json_decode(file_get_contents(__DIR__ . '/content/publications.json'), true);
					usort($publications, function ($a, $b) {
						if ((int) $a['date'] == (int) $b['date']) {
							return 0;
						}
						return ((int) $a['date'] < (int) $b['date']) ? 1 : -1;
					});
					foreach ($publications as $idx => $publication) {
						echo '<li>';
						echo $publication['authors'] . '.<br>';
						echo '<i>' . $publication['title'] . '.</i><br>';
						echo $publication['publisher'] . ', ' . $publication['date'] . '.';
						if ($publication['bibtex'] !== null) {
							echo ' [ <a href="javascript: void(0);" data-toggle="modal" data-target="#publication' . $idx . '"><i class="fas fa-quote-right"></i> BibTex</a> ]';
						}
						if (strlen($publication['pdf'])) {
							echo ' [ <a href="' . $publication['pdf'] . '" target="_blank"><i class="far fa-file-pdf"></i> PDF</a> ]';
						}
						if (strlen($publication['doi'])) {
							echo ' [ <a href="https://doi.org/' . $publication['doi'] . '" target="_blank"><i class="fas fa-link"></i> DOI</a> ]';
						}
						if (strlen($publication['repository'])) {
							echo ' [ <a href="' . $publication['repository'] . '" target="_blank"><i class="fas fa-code"></i> Code</a> ]';
						}
						echo '</li>';
					}
				?>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="summary"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Summary</h2>
				<?php include __DIR__ . '/content/summary.html'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<a name="contact"></a>
				<h2 class="display-4"><i class="fas fa-angle-right"></i> Contact</h2>
				<?php include __DIR__ . '/content/contact.html'; ?>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container text-center">
			Project No. 2016/21/B/ST6/02158 Investigators &copy; <?php echo date('Y'); ?>
		</div>
	</footer>
	<a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
	<?php
	foreach ($publications as $idx => $publication) {
		if ($publication['bibtex'] !== null) {
			echo '<div class="modal fade" id="publication' . $idx . '" tabindex="-1">';
			echo '<div class="modal-dialog modal-lg">';
			echo '<div class="modal-content">';
			echo '<div class="modal-header">';
			echo '<h5 class="modal-title">Cite</h5>';
			echo '</div>';
			echo '<div class="modal-body"><pre><code>';
			if (is_array($publication['bibtex'])) {
				echo implode("\n", $publication['bibtex']);
			} else {
				echo $publication['bibtex'];
			}
			echo '</code></pre></div>';
			echo '<div class="modal-footer">';
			echo '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
	}
	?>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script>
		$(document).ready(function(){
			$(window).scroll(function () {
				if ($(this).scrollTop() > 50) {
					$('#back-to-top').show();
				} else {
					$('#back-to-top').hide();
				}
			});
			$('#back-to-top').click(function () {
				$('body,html').scrollTop(0);
				return false;
			});
		});
	</script>
</body>
</html>
<?php
// ends getting HTML output
$out = ob_get_clean();
// saves the output
file_put_contents($dir . '/index.html', $out);
